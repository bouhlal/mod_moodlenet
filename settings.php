<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings used by the MoodleNet module
 *
 * @package mod_moodlenet
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2019 Mayel de Borniol  {@link http://mayel.space}
 *
 **/

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once(__DIR__ . '/classes/adminlib.php');

    $settings->add(new admin_setting_configtext('mod_moodlenet/domainurl', get_string('domainurl', 'mod_moodlenet'), '',
            'https://app.next.moodle.net', PARAM_RAW_TRIMMED));
    // Allowed mimetypes.
    $settings->add(new mod_moodlenet_allowedmimetypes(
            'mod_moodlenet/allowedmimetypes', new lang_string('allowedmimetypes', 'mod_moodlenet'),
            new lang_string('allowedmimetypesdesc', 'mod_moodlenet'),
            'text/xml;image/png;application/pdf;text/plain;application/vnd.moodle.backup;application/octet-stream'));

    // Allowed extensions.
    $settings->add(new mod_moodlenet_allowedextensions(
            'mod_moodlenet/allowedextensions', new lang_string('allowedextensions', 'mod_moodlenet'),
            new lang_string('allowedextensionssdesc', 'mod_moodlenet'),
            'pdf;rtf;docx;doc;odt;ott;xls;xlsx;ods;ots;csv;ppt;pps;pptx;odp;otp;odg;otg;odc;ogg;mp3;m4a;wav;mp4;flv;avi;gif;jpg;jpeg;png;svg;webm;eps;tex;mbz'));

}
